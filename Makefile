BUILD_DIR := build

ARCHIVE_SRC := app.c
ARCHIVE_OBJ := $(patsubst %.c,$(BUILD_DIR)/%.o, $(ARCHIVE_SRC))
ARCHIVE := $(BUILD_DIR)/libmultiplexer.a

TEST_SRC := test.c
TEST_OBJ := $(patsubst %.c,%.o, $(TEST_SRC))
TEST_BIN := $(BUILD_DIR)/test

CCFLAGS := -O3 -DNUMBER_OF_APPS=20

TEST_FLAGS := -DTEST_PORT=20202 -DTEST_APP_ID=10

.PHONY: $(ARCHIVE_OBJ) $(ARCHIVE) $(TEST_OBJ) $(TEST_BIN)

all: $(BUILD_DIR) $(ARCHIVE) $(TEST_BIN)

$(ARCHIVE): $(ARCHIVE_OBJ)
	ar rcs $@ $<

$(ARCHIVE_OBJ):
	gcc $(CCFLAGS) -c $(patsubst $(BUILD_DIR)/%.o,%.c, $@) -o $@

$(TEST_BIN):
	gcc $(TEST_FLAGS) $(CCFLAGS) $(TEST_SRC) $(ARCHIVE) -o $@

$(BUILD_DIR):
	mkdir -p $@

clean:
	rm -rf build
