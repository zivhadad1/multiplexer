#ifndef MULTIPLEXER_LOG_H
#define MULTIPLEXER_LOG_H

#include <stdio.h>
#define LOG(fmt, ...) printf("%s: %s: %d: " fmt "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);

#endif // MULTIPLEXER_LOG_H
