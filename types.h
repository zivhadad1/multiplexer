#ifndef MULTIPLEXER_TYPES_H
#define MULTIPLEXER_TYPES_H

#include <stdint.h>
#include <stdbool.h>


typedef uint32_t app_id_t;

struct app;

typedef bool(*app_init_t)(struct app *);
typedef bool(*app_invoke_t)(struct app *, const char *, size_t);
typedef bool(*app_term_t)(struct app *);

struct app {
    app_init_t init;
    app_invoke_t invoke;
    app_term_t term;

    // Used by the application when using other resources (?)
    app_id_t id;
};

#endif // MULTIPLEXER_TYPES_H
