import socket
import construct

### General Application Message
# struct __attribute__((__packed__)) app_message {
#     uint8_t app_id;
#     char data[0];
# };

### Send UDP Application Message
# struct __attribute__((__packed__)) send_udp_app_message {
#     uint32_t to_ip;
#     uint16_t to_port;

#     uint32_t data_size;
#     char data[0];
# };


app_message = construct.Struct(
    "app_id" / construct.Int8ul,
    "data" / construct.GreedyBytes
)

send_udp_app_message = construct.Struct(
    "to_ip" / construct.Int32ul,
    "to_port" / construct.Int16ul,

    "data_size" / construct.Rebuild(construct.Int32ul, construct.len_(construct.this.data)),
    "data" / construct.Byte[construct.this.data_size]
)


while True:
    ip = input("IP: ")
    port = int(input("Port: "))
    message = input("Message: ").encode("utf-8")

    ip_int = 0
    for seg in ip.split("."):
        ip_int = ip_int * 0x100 + int(seg)

    data = send_udp_app_message.build(dict(
        to_ip=ip_int,
        to_port=port,
        data=message
    ))

    data = app_message.build(dict(
        app_id=10,
        data=data
    ))

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto(data, ("127.0.0.1", 20202))
