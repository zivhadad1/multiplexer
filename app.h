#ifndef MULTIPLEXER_APP_H
#define MULTIPLEXER_APP_H

#include "types.h"


/*
 * Initialize the applications manager
 *
 * @returns True if successful, false otherwise
 */
bool app_manager_init(void);

/*
 * Install an application with ID
 *
 * @param   app_id - application ID
 * @param   app - application to install
 *
 * @returns True if successful, false otherwise
 */
bool app_manager_install_app(app_id_t app_id, struct app *app);

/*
 * Uninstall an application by ID
 *
 * @param   app_id - application ID
 *
 * @returns True if successful, false otherwise
 */
bool app_manager_uninstall_app(app_id_t app_id);

/*
 * Invoke application by ID with data
 *
 * @param   app_id - application ID
 * @param   data
 * @param   data_size
 *
 * @returns True if successful, false otherwise
 */
bool app_manager_invoke_app(app_id_t app_id, char *data, size_t data_size);

/*
 * Terminate the applications manager
 *
 * @returns True if successful, false otherwise
 */
bool app_manager_term(void);

#endif // MULTIPLEXER_APP_H
