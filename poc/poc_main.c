#include <time.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>


#define LOG(fmt, ...) printf("%s: %s: %d: " fmt "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);

#define TEST_PORT 5000

struct channel_data;

typedef void(*cb_t)(struct channel_data *);

struct __attribute__((__packed__)) channel_data {
    uint8_t channel_num;
    uint8_t data_size;
    uint8_t data[0];
};

size_t callback_0_counter = 0;
void callback_0(struct channel_data *data)
{
    callback_0_counter += data->data_size;
    printf("Callback %d received %d bytes so far\n", data->channel_num, callback_0_counter);
}

void callback_1(struct channel_data *data)
{
    data->data[data->data_size] = 0;
    printf("Callback %d: %s(%d)\n", data->channel_num, data->data, data->data_size);
}


cb_t g_callbacks[] = {
    callback_0,
    callback_1
};


int main()
{
    srand(time(0));
    LOG("Test starts");

    int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (-1 == sock) {
        LOG("sock failed: %s", strerror(errno));
        goto exit;
    }

    struct sockaddr_in server_addr = {
        .sin_family = AF_INET,
        .sin_port = htons(TEST_PORT),
        .sin_addr.s_addr = 0
    };
    if (-1 == bind(sock, (const struct sockaddr *)&server_addr, sizeof(server_addr))) {
        LOG("bind failed: %s", strerror(errno));
        goto close_socket;
    }

    while (true) {
        char buffer[100];

        ssize_t bytes_read = recv(sock, buffer, sizeof(buffer), 0);
        if (-1 == bytes_read) {
            LOG("recvfrom failed: %s", strerror(errno));
            goto close_socket;
        }

        struct channel_data *channel_data = (struct channel_data *)buffer;

        if (sizeof(g_callbacks) / sizeof(g_callbacks[0]) < channel_data->channel_num) {
            LOG("Channel number too big");
            continue;
        }

        // call the callback
        g_callbacks[channel_data->channel_num](channel_data);
    }

close_socket:
    close(sock);

exit:
    LOG("Test ends");
    return 0;
}
