#include <stdlib.h>

#include "log.h"

#include "app.h"


static struct app *g_installed_apps[NUMBER_OF_APPS] = { 0 };


static inline struct app **get_app_slot(app_id_t app_id)
{
    if (NUMBER_OF_APPS > app_id)
    {
        return &g_installed_apps[app_id];
    }
    return NULL;
}

static inline struct app *get_app(app_id_t app_id)
{
    if (NUMBER_OF_APPS > app_id)
    {
        return g_installed_apps[app_id];
    }
    return NULL;
}

static inline bool set_app_slot(app_id_t app_id, struct app *app)
{
    struct app **slot = get_app_slot(app_id);
    if (NULL == slot) {
        goto error;
    }

    *slot = app;

    return true;

error:
    return false;
}

static inline bool is_app_slot_free(app_id_t app_id)
{
    return NULL == get_app(app_id);
}


bool app_manager_init(void)
{
    return true;
}

bool app_manager_install_app(app_id_t app_id, struct app *app)
{
    // Check if the application slot is free
    if (!is_app_slot_free(app_id)) {
        LOG("Slot %d is not free", app_id);
        goto error;
    }

    // Occupy the application slot
    if (!set_app_slot(app_id, app)) {
        LOG("Setting slot %d failed", app_id);
        goto error;
    }

    // Initialize the application
    app->id = app_id;
    if (!app->init(app)) {
        LOG("Initializing application of slot %d failed", app_id);
        goto free_app_slot;
    }

    return true;

free_app_slot:
    set_app_slot(app_id, NULL);

error:
    return false;
}

bool app_manager_uninstall_app(app_id_t app_id)
{
    // Check if the application slot is used
    if (is_app_slot_free(app_id)) {
        LOG("Slot %d is not occupied by application", app_id);
        goto error;
    }

    struct app *app = get_app(app_id);

    // Free the application slot
    if (!set_app_slot(app_id, NULL)) {
        LOG("Setting slot %d failed", app_id);
        goto error;
    }

    // Terminate the application
    if (!app->term(app)) {
        LOG("Terminating application of slot %d failed", app_id);
        goto error;
    }

    return true;

error:
    return false;
}

bool app_manager_invoke_app(app_id_t app_id, char *data, size_t data_size)
{
    // Check if slot is used
    if (is_app_slot_free(app_id)) {
        LOG("Slot %d is not occupied by application", app_id);
        goto error;
    }

    struct app *app = get_app(app_id);
    return app->invoke(app, data, data_size);;

error:
    return false;
}

bool app_manager_term(void)
{
    bool ret = true;
    for (int i = 0; i < NUMBER_OF_APPS; ++i) {
        if (!is_app_slot_free(i)) {
            struct app *app = get_app(i);
            ret &= app->term(app);
        }
    }

    return ret;
}
