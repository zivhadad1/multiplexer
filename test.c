#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>

#include "log.h"
#include "app.h"


struct __attribute__((__packed__)) send_udp_app_message {
    uint32_t to_ip;
    uint16_t to_port;

    uint32_t data_size;
    char data[0];
};

struct send_udp_app {
    struct app base;
    int sock;
};

bool send_udp_init(struct send_udp_app *self);

bool send_udp_invoke(struct send_udp_app *self, struct send_udp_app_message *message, size_t data_size);

bool send_udp_term(struct send_udp_app *self);


struct send_udp_app send_udp = {
    .base = {
        .init = (app_init_t)send_udp_init,
        .invoke = (app_invoke_t)send_udp_invoke,
        .term = (app_term_t)send_udp_term
    }
};


struct __attribute__((__packed__)) app_message {
    uint8_t app_id;
    char data[0];
};

int main()
{
    LOG("Test starts");

    if (!app_manager_init()) {
        LOG("app_manager_init failed");
        goto exit;
    }

    if (!app_manager_install_app(TEST_APP_ID, (struct app *)&send_udp)) {
        LOG("Installing app failed");
        goto shutdown_app_manager;
    }

    int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (-1 == sock) {
        LOG("sock failed: %s", strerror(errno));
        goto exit;
    }

    struct sockaddr_in server_addr = {
        .sin_family = AF_INET,
        .sin_port = htons(TEST_PORT),
        .sin_addr.s_addr = 0
    };
    if (-1 == bind(sock, (const struct sockaddr *)&server_addr, sizeof(server_addr))) {
        LOG("bind failed: %s", strerror(errno));
        goto close_socket;
    }

    LOG("Receiving socket bound to port %d", TEST_PORT);

    while (true) {
        char buffer[100];

        ssize_t bytes_read = recv(sock, buffer, sizeof(buffer), 0);
        if (-1 == bytes_read) {
            LOG("recvfrom failed: %s", strerror(errno));
            goto close_socket;
        }

        struct app_message *app_message = (struct app_message *)buffer;

        if (!app_manager_invoke_app(app_message->app_id, app_message->data, bytes_read - sizeof(app_message->app_id))) {
            LOG("Invoking app failed");
        }
    }

close_socket:
    close(sock);

shutdown_app_manager:
    app_manager_term();

exit:
    LOG("Test ends");
    return 0;
}


bool send_udp_init(struct send_udp_app *self)
{
    self->sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (-1 == self->sock) {
        goto error;
    }

    return true;

error:
    return false;
}

bool send_udp_invoke(struct send_udp_app *self, struct send_udp_app_message *message, size_t data_size)
{
    struct sockaddr_in remote_addr = {
        .sin_family = AF_INET,
        .sin_port = htons(message->to_port),
        .sin_addr.s_addr = htonl(message->to_ip)
    };

    ssize_t res = sendto(self->sock, message->data, message->data_size, 0, (const struct sockaddr *)&remote_addr, sizeof(remote_addr));
    if (-1 == res) {
        LOG("Error sending data to %x:%x, error: %s", message->to_ip, message->to_port, strerror(errno));
    }

    return res != -1;
}

bool send_udp_term(struct send_udp_app *self)
{
    close(self->sock);
    return true;
}
